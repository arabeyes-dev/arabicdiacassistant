#!/usr/bin/python
# -*- coding=utf-8 -*-
from aranalex.stem_noun import *
from aranalex.stem_verb import *
from core.stopwords import *
class analex :

	def __init__(self):
		self.nounstemmer=nounStemmer();
		self.verbstemmer=verbStemmer();
		self.debug=False;
		self.limit=10000;
#-----------------------------------
	def text_treat(self,text):
		return text;
	def set_debug(self,debug):
		self.debug=debug;
	def set_limit(self,limit):
		self.limit=limit;
# التحقق من وجود الزائدة المكونة من السابقة واللاحقة
# التحقق دون شكل
# التحقق بالشكل
# تشكيل الزوائد و استخلاص الحالات الممكنة
# إذا كانت الزائدة مشكولة شكلا مرفوضا ترفض
# حالات الزوائد المشكولة جزئيا : للعمل.
#-----------------------------------
	def check_text(self,text, mode='all'):
		text=self.text_treat(text);
		text=araby.stripHarakat(text);
		als=tashaphyne.ArabicLightStemmer();
		list_word=als.tokenize(text);
		resulted_text=u""
		resulted_data=[];
		if mode=='all':
			for word in list_word [:self.limit]:
				one_data_list=self.check_word(word);
				resulted_data.append(one_data_list);
		elif mode=='nouns':
			for word in list_word[:self.limit] :
				one_data_list=self.check_word_as_noun(word);
				resulted_data.append(one_data_list);
		elif mode=='verbs':
			for word in list_word[:self.limit] :
				one_data_list=self.check_word_as_verb(word);
				resulted_data.append(one_data_list);
		return resulted_data;


	def check_text_as_nouns(self,text):
		return self.check_text(text,"nouns");
	def check_text_as_verbs(self,text):
		return self.check_text(text,"verbs");
#---------------------------------------
# التحقق من وجود الزائدة المكونة من السابقة واللاحقة
# التحقق دون شكل
# التحقق بالشكل
# تشكيل الزوائد و استخلاص الحالات الممكنة
# إذا كانت الزائدة مشكولة شكلا مرفوضا ترفض
# حالات الزوائد المشكولة جزئيا : للعمل.
#-----------------------------------
	def check_word(self,word):
# ToDo
# vocalised word and unvocalised word
		word_vocalised=word;
		resulted_text=u"";
		resulted_data=[];
		word=ar_strip_marks_keepshadda(word);

		# if word is stopword
		resulted_data+=self.check_word_as_stopword(word);

		if len(resulted_data)==0:
			#TodDo guessing word type

			#if word is verb
			resulted_data+=self.check_word_as_verb(word);

			#if word is noun
			resulted_data+=self.check_word_as_noun(word);
		if len(resulted_data)==0:
			resulted_data.append({
			'word':word,
			'procletic':'',
			'encletic':'',
			'prefix':'',
			'suffix':'',
			'stem':'',
			'original':'',
			'vocalized':'',
			'tags':u'',
			'type':'unknown',
			'root':'',
			'template':'',
			});
		return resulted_data;

	def check_word_as_stopword(self,word):
		detailed_result=[]
		if STOPWORDS.has_key(word):
			detailed_result.append({
			'word':word,
			'procletic':'',
			'encletic':'',
			'prefix':'',
			'suffix':'',
			'stem':STOPWORDS[word],
			'original':STOPWORDS[word],
			'vocalized':STOPWORDS[word],
			'tags':u'أداة',
			'type':'STOPWORD',
			'root':'',
			'template':'',
			});
		return detailed_result
	def check_word_as_verb(self,verb):
		list_found=self.verbstemmer.stemming_verb(verb,self.debug)
		#print verb.encode("utf8")+"\t"+str(len(list_found))+'\t['+(u'\t'.join(set(list_found))).encode("utf8")+"]";
		detailed_result=list_found;
		#for detailed in detailed_result:
		#	for key in detailed.keys():
		#		print key,detailed[key].encode('utf8'),
		#	print;
		return detailed_result;
	def check_word_as_noun(self,noun):
		list_found=self.nounstemmer.stemming_noun(noun,self.debug)
		#print noun.encode("utf8")+"\t"+str(len(list_found))+'\t['+(u'\t'.join(set(list_found))).encode("utf8")+"]";
		#print "-----------------",noun.encode('utf8'),"------------"
		detailed_result=list_found;
		#for detailed in detailed_result:
		#	for key in detailed.keys():
		#		print key,detailed[key].encode('utf8'),
		#	print ;
		return detailed_result;

